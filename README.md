# README #

## Learn and Code Task Distribution System Project -by Manishi


# Dependencies
* dotnet >= 3.1
* MysqlData >= 8.0.27
  
# How to run Program
* Go to the root directory of the project.
*  Navigate to src/app
*  Go to Task Distributor Directory and run `dotnet run`
*  Go to Node Directory and run `dotnet run`
*  To Execute Task : Go to Client Directory and run `dotnet run execute filename`
*  To Query Task : Go to Client Directory and run `dotnet run query task id`
* Now The Results will be saved to database and will be shown on the console.
* Logs will be generated at src/app/logs.txt

# How to run Tests:
* Go to the root directory of the project.
*  Navigate to src/tests
*  Simply run `dotnet test -v=n` to see the output in verbose mode.