﻿using System;
using Distributer.DAO.Task;
using Utils;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandFactory.SetCommand(args);
        }
    }
}