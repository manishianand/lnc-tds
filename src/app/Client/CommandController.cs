using System;
using Utils;

namespace Client
{
    public class CommandController
    {
        public static void QueueTask(string[] args)
        {
            var file = FileServices.ParseFileName(args[1]);
            if (file == null)
            {
                Console.WriteLine(Error.invalidFileError);
                return;
            }
            string requestString = CommandExecutor.BuildQuery(file);
            if (requestString == Error.fileNotFoundError)
            {
                Console.WriteLine(Error.fileNotFoundError);
                return;
            }
            CommandExecutor.ExecuteClient(requestString);
        }
    }
}