using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Distributer.DAO.Client;
using Distributer.DAO.Task;
using Utils;

namespace Client
{
    public static class CommandExecutor
    {
        public static string BuildQuery(IDictionary<string, string> file)
        {
            var filePath = FileServices.GetBaseDirectory() + "\\" + file["name"].Trim();
            Request request = new Request();
            request.payload = new ClientDTO();
            request.payload.Task = new TaskDTO();
            if (File.Exists(filePath))
            {
                IEnumerable<string> text = File.ReadLines(filePath);
                request.payload.Task.task = text;
                request.payload.Task.taskType = file["extention"];
                request.payload.taskId = Guid.NewGuid().ToString();
                request.payload.port = Constants.clientPort.ToString();
                request.payload.Task.id = request.payload.taskId;
                string requestString = ObjectSerializer.Serialize(request);
                Console.WriteLine($"Task Id for current task : {request.payload.taskId}");
                return requestString;
            }
            else
            {
                return Error.fileNotFoundError;
            }
        }

        public static void ExecuteClient(string requestString)
        {
            try
            {
                IPAddress ipAddress = IPAddress.Parse(Constants.ipAddress);
                IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Constants.clientPort);
                Socket client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                client.Connect(localEndPoint);
                Console.WriteLine($"Client connected to : {client.RemoteEndPoint.ToString()}");
                Logger.Info($"Client connected to : {client.RemoteEndPoint.ToString()}");
                byte[] byteRequestMessage = FileServices.ConstructByteMessage(requestString);
                int byteSent = client.Send(byteRequestMessage);
                // Data buffer
                byte[] messageReceived = new byte[Constants.bufferSize];
                int byteRecv = client.Receive(messageReceived);
                if (requestString == "execute")
                    Console.WriteLine($"Task Result : { Encoding.ASCII.GetString(messageReceived, 0, byteRecv)}");
                client.Shutdown(SocketShutdown.Both);
                client.Close();
                Logger.Info("Client Closed");
            }

            catch (ArgumentNullException argumentNullException)
            {
                Console.WriteLine($"ArgumentNullException : {argumentNullException.Message}");
                Logger.Error($"ArgumentNullException : {argumentNullException.Message}");
            }

            catch (SocketException socketException)
            {
                Console.WriteLine($"SocketException : {socketException.Message}");
                Logger.Error($"SocketException : {socketException.Message}");
            }

            catch (Exception exception)
            {
                Console.WriteLine($"Unexpected exception : {exception.Message}");
                Logger.Error($"Unexpected exception : {exception.Message}");
            }
        }
    }
}