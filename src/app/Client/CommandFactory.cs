using System;
using Utils;
namespace Client
{
    public class CommandFactory
    {
        public static void SetCommand(string[] args)
        {
            switch (args[0])
            {
                case "query":
                    QueryExecutor.GetTaskDetailsByGuid(args[1]);
                    break;

                case "queue":
                    CommandController.QueueTask(args);
                    Console.WriteLine("Task Queued");
                    break;

                case "execute":
                    CommandExecutor.ExecuteClient(Constants.command.execute.ToString());
                    break;

                default:
                    Console.WriteLine("Please Enter a valid command !\n1.]dotnet run execute <file> \n2.]dotnet run query <guid>");
                    break;
            }
        }
    }
}