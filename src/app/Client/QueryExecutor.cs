using System;
using Distributer.DAO.Task;

namespace Client
{
    public static class QueryExecutor
    {
        public static void GetTaskDetailsByGuid(string guid)
        {
            TaskImpl taskImpl = new TaskImpl();
            var task = taskImpl.GetTaskById(guid);
            Console.WriteLine($" Id: {task.id}\n Type: {task.taskType}\n Node ID: {task.nodeId}\n Task Status: {task.status}\n Result: {task.result}");
        }
    }
}