using Distributer.DAO.Task;

namespace Node
{
    public class ExecutorFactory
    {
        public static string GetExecutor(TaskDTO task)
        {
            switch (task.taskType)
            {
                case "py": return PythonExecutor.ExecPython(task.task, task.id);
                case "go": return GoExecutor.ExecGO(task.task, task.id);
                case "js": return JsExecutor.ExecJS(task.task, task.id);
            }
            return null;
        }
    }
}