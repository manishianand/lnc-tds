using Utils;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Node
{
    public static class GoExecutor
    {
       
        public static string ExecGO(IEnumerable<string> data, string taskId)
        {
            string folder = Directory.GetCurrentDirectory();
            string fileName = Utils.Constants.goLangFile;
            string fullPath = folder + fileName;
            File.WriteAllLines(fullPath, data);
            Logger.Info($"Output Saved for task: {taskId}");
            string command = Utils.Constants.executeGoLang;
            var process = Process.Start("cmd.exe", "/C " + command);
            return ProcessController.WriteResult(process, folder, taskId);
        }
        
        
    }
}