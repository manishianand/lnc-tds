using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Utils;

namespace Node
{
    public class JsExecutor
    {
        public static string ExecJS(IEnumerable<string> data, string taskId)
        {
            string folder = Directory.GetCurrentDirectory();
            string fileName = Utils.Constants.jsFile;
            string fullPath = folder + fileName;
            File.WriteAllLines(fullPath, data);
            Logger.Info($"Output Saved for task: {taskId}");
            string command = Utils.Constants.executeJS;
            var process = Process.Start("cmd.exe", "/C " + command);
            return ProcessController.WriteResult(process, folder, taskId);
        }
    }
}