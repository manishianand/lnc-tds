﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Distributer.DAO.Task;
using Utils;

namespace Node
{
    class Node
    {
        static void Main(string[] args)
        {
            ExecuteServer();
        }

        public static void ExecuteServer()
        {

            IPAddress ipAddress = IPAddress.Parse(Constants.ipAddress);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Constants.serverPort);
            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                while (true)
                {
                    Console.WriteLine("\nWaiting for Connections ... ");
                    Socket clientSocket = listener.Accept();
                    // Data buffer
                    byte[] buffer = new Byte[Constants.bufferSize];
                    string data = null;
                    while (true)
                    {
                        int byteSize = clientSocket.Receive(buffer);
                        data += Encoding.ASCII.GetString(buffer, 0, byteSize);
                        break;
                    }
                    var task = ObjectSerializer.Deserialize<TaskDTO>(data);
                    Console.WriteLine($"Task received with Id-> {task.id} ");
                    Logger.Info($"Task received with id {task.id}");
                    var res = ExecutorFactory.GetExecutor(task);
                    Console.WriteLine("\n[i] Task execution finished [i]");
                    Logger.Info("\nTask execution finished ...");
                    byte[] message = Encoding.ASCII.GetBytes(res);
                    // Send a message to Client
                    clientSocket.Send(message);
                    clientSocket.Shutdown(SocketShutdown.Both);
                    clientSocket.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
