using System;
using System.Diagnostics;
using System.IO;
using Utils;

namespace Node

{
    public static class ProcessController
    {
        public static string WriteResult(Process process, string outputDirectory, string taskId)
        {
            string outputFile = Utils.Constants.outputFile;
            var timeFrame = DateTime.Now.AddSeconds(5);
            var result = String.Empty;
            while (true)
            {
                if (!process.HasExited && DateTime.Now >= timeFrame)
                {
                    process.Close();
                    return Constants.taskStatus.TLE.ToString();
                }
                else if (process.HasExited && process.ExitCode != 0)
                {
                    Logger.Info($"Task {taskId} Exited with status code {process.ExitCode}");
                    result = File.ReadAllText(outputDirectory + outputFile);
                    return "Fault :" + result;
                }
                else if (process.HasExited)
                {
                    break;
                }
            }
            result = File.ReadAllText(outputDirectory + outputFile);
            return result;
        }
    }
}