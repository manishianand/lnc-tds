using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Utils;

namespace Node
{
    public class PythonExecutor
    {
        public static string ExecPython(IEnumerable<string> data, string taskId)
        {
            string folder = Directory.GetCurrentDirectory();
            string fileName = Utils.Constants.pythonFile;
            string fullPath = folder + fileName;

            File.WriteAllLines(fullPath, data);
            Logger.Info($"Output Saved for task: {taskId}");
            string command = Utils.Constants.executePython;
            var process = Process.Start("cmd.exe", "/C " + command);
            return ProcessController.WriteResult(process, folder, taskId);
        }
    }
}