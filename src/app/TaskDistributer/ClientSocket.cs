﻿using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Distributer.DAO.Client;
using Utils;
using Distributer.DAO.Task;

namespace Distributer
{
    class ClientSocket
    {

        private static Queue<ClientDTO> _taskQueue = new Queue<ClientDTO>();
        static void Main(string[] args)
        {
            CreateServerSocket(Constants.clientPort);
        }
        public static string CreateServerSocket(int port)
        {
            IPAddress ipAddress = IPAddress.Parse(Constants.ipAddress);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);
            Socket clientListener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                clientListener.Bind(localEndPoint);
                clientListener.Listen(10);
                string requestStream = null;
                while (true)
                {
                    Console.WriteLine("\nWaiting for tasks... ");
                    Socket clientSocket = clientListener.Accept();
                    byte[] buffer = new Byte[Constants.bufferSize];
                    while (true)
                    {
                        int byteSize = clientSocket.Receive(buffer);
                        requestStream += Encoding.ASCII.GetString(buffer, 0, byteSize);
                        break;
                    }
                    if (requestStream == Constants.command.execute.ToString())
                    {
                        TaskController.ExecuteTask(clientSocket, _taskQueue);
                    }
                    else
                    {
                        TaskController.QueueTask(clientSocket, requestStream, ipAddress, _taskQueue);
                    }
                    requestStream = null;
                    clientSocket.Shutdown(SocketShutdown.Both);
                    clientSocket.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Logger.Error(e.Message);
                return null;
            }
        }
    }
}