using System;
using Distributer.DAO.Task;

namespace Distributer.DAO.Client
{
    public class Request
    {
        public string type = "request";
        public object header = new
        {
            TDSProtocolVersion = "1.0",
            TDSProtocolFormat = "json"
        };
        public ClientDTO payload { get; set; }
    }
    public class ClientDTO
    {
        public int id { get; set; }
        public string ip { get; set; }
        public string port { get; set; }
        public string taskId { get; set; }
        public TaskDTO Task { get; set; }
    }
}