using Microsoft.VisualBasic.CompilerServices;
using System.Collections.Generic;
using System;
using MySql.Data.MySqlClient;
using Utils;

namespace Distributer.DAO.Client

{
    public class ClientImpl : IClient
    {
        private static string _getClientByIdQuery = "SELECT * FROM CLIENT WHERE id =@id;";
        private static string _insertClientQuery = "INSERT INTO Client(id,ip,port,taskId) VALUES (@id,@ip,@port,@taskId);";
        public List<ClientDTO> GetClients()
        {

            throw new NotImplementedException();
        }
        public ClientDTO GetClientById(int id)
        {
            ClientDTO payload = new ClientDTO();
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }
                var command = new MySqlCommand(_getClientByIdQuery, databaseConnection);
                command.Parameters.AddWithValue(@"id", id);
                var reader = command.ExecuteReader();
                reader.Read();
                payload.id = (int)reader[0];
                payload.port = reader[1].ToString();
                payload.ip = reader[2].ToString();
                payload.taskId = (string)reader[3];

            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception.Message}");
                Logger.Error($"Error : {exception.Message}");
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return payload;
        }

        public bool InsertClient(ClientDTO payload)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }

                MySqlDataAdapter commandAdapter = new MySqlDataAdapter();
                commandAdapter.InsertCommand = new MySqlCommand(_insertClientQuery, databaseConnection);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"port", payload.port);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"id", payload.id);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"ip", payload.ip);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"taskId", payload.taskId);
                commandAdapter.InsertCommand.ExecuteNonQuery();
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception}");
                Logger.Error($"Error : {exception}");
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return true;
        }
    }
}