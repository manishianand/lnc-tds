using System;
using System.Collections.Generic;
namespace Distributer.DAO.Client

{
    public interface IClient
    {
        public List<ClientDTO> GetClients();
        public ClientDTO GetClientById(int id);
        public bool InsertClient(ClientDTO clientDTO);
        enum CommandType
        {
            Query,
            NonQuery
        }
    }
}