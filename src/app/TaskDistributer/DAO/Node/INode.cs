using System.Collections.Generic;
using Distributer.DAO.Task;

namespace Distributer.DAO.Node
{
    public interface INode
    {
        public List<NodeDTO> GetNodes();
        public NodeDTO GetNodeById(int id);
        public bool InsertNode(NodeDTO client, TaskDTO task);
    }
}