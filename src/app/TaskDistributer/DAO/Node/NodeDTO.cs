using System;
namespace Distributer.DAO.Node
{
    public class NodeDTO
    {
        public int id { get; set; }
        public string status { get; set; }
        public string taskId { get; set; }
        public string ip { get; set; }
        public string capability { get; set; }
        public string port { get; set; }
    }
}