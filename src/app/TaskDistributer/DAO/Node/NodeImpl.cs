using System.Collections.Generic;
using System;
using MySql.Data.MySqlClient;
using Utils;
using Distributer.DAO.Task;

namespace Distributer.DAO.Node
{
    public class NodeImpl : INode
    {
        private string _getNodeByIdQuery = "SELECT * FROM NODE WHERE id =@id;";
        private string _insertNodeQuery = "INSERT INTO NODE(id,ip,port,status,capability,taskId) VALUES (@id,@ip,@port,@status,@capability,@taskId);";
        private string _updateNodeStatusQuery = "UPDATE NODE SET status=@status where taskId=@taskId";
        public List<NodeDTO> GetNodes()
        {

            throw new NotImplementedException();
        }
        public NodeDTO GetNodeById(int id)
        {
            NodeDTO node = new NodeDTO();
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }
                var command = new MySqlCommand(_getNodeByIdQuery, databaseConnection);
                command.Parameters.AddWithValue(@"id", id);
                var reader = command.ExecuteReader();
                reader.Read();
                node.id = (int)reader[0];
                node.ip = reader[1].ToString();
                node.port = reader[2].ToString();
                node.status = reader[3].ToString();
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception.Message}");
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return node;
        }
        public bool InsertNode(NodeDTO node, TaskDTO task)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }

                MySqlDataAdapter commandAdapter = new MySqlDataAdapter();
                commandAdapter.InsertCommand = new MySqlCommand(_insertNodeQuery, databaseConnection);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"port", node.port);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"id", node.id);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"ip", node.ip);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"status", node.status);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"capability", node.capability);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"taskId", task.id);
                commandAdapter.InsertCommand.ExecuteNonQuery();
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception}");
                Logger.Error($"Error : {exception}");
                return false;
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return true;
        }
        public bool UpdateNodeStatus(String taskId, string status)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }

                MySqlDataAdapter commandAdapter = new MySqlDataAdapter();
                commandAdapter.InsertCommand = new MySqlCommand(_updateNodeStatusQuery, databaseConnection);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"status", status);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"taskId", taskId);
                commandAdapter.InsertCommand.ExecuteNonQuery();
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception}");
                Logger.Error($"Error : {exception}");
                return false;
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return true;
        }
    }
}