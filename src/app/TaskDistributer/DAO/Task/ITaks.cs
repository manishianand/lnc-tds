using System;
using System.Collections.Generic;

namespace Distributer.DAO.Task
{
    public interface ITask
    {
        List<TaskDTO> GetTasks();
        TaskDTO GetTaskById(string id);
        bool InsertTask(TaskDTO client);
    }
}