using System.Collections;
using System;
using System.Collections.Generic;

namespace Distributer.DAO.Task
{
    public class TaskDTO
    {
        public string id { get; set; }
        public string status { get; set; }
        public IEnumerable<string> task { get; set; }
        public string result { get; set; }
        public string taskType { get; set; }
        public int? nodeId { get; set; }
    }
}