using System.Collections.Generic;
using System.Data;
using System;
using MySql.Data.MySqlClient;
using Utils;

namespace Distributer.DAO.Task
{
    public class TaskImpl : ITask
    {
        private string _getTaskByIdQuery = "SELECT * FROM TASK WHERE id =@id;";
        private string _insertTaskQuery = "INSERT INTO TASK(id,result,nodeId,status,type) VALUES (@id,@result, @nodeID, @status, @type);";
        private string _updateTaskStatusQuery = "UPDATE TASK SET status=@status where id=@taskId";
        private string _updateTaskResultQuery = "UPDATE TASK SET result=@result where id=@taskId";
        private string _updateTaskNodeQuery = "UPDATE TASK SET nodeId=@nodeId where id=@taskId";

        public bool InsertTask(TaskDTO newTask)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }
                MySqlDataAdapter commandAdapter = new MySqlDataAdapter();
                commandAdapter.InsertCommand = new MySqlCommand(_insertTaskQuery, databaseConnection);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"id", newTask.id);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"result", newTask.result);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"nodeId", newTask.nodeId);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"type", newTask.taskType);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"status", newTask.status);
                commandAdapter.InsertCommand.ExecuteNonQuery();
            }
            catch (MySqlException exception)
            {
                Logger.Error($"Error : {exception.Message}");
                return false;
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return true;
        }

        public TaskDTO GetTaskById(string taskId)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            TaskDTO task = new TaskDTO();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }
                var command = new MySqlCommand(_getTaskByIdQuery, databaseConnection);
                command.Parameters.AddWithValue(@"id", taskId);
                var reader = command.ExecuteReader();
                reader.Read();
                task.id = (string)reader[0];
                task.result = reader[1] == DBNull.Value? "N/A": reader[1].ToString();
                task.nodeId = reader[2] == DBNull.Value ? 0 : (int)reader[2];
                task.status = reader[3].ToString();
                task.taskType = reader[4].ToString();
                return task;
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception.Message}");
                Logger.Error($"Error : {exception.Message}");
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return task;
        }
        public bool UpdateTaskStatus(String taskId, string status)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }

                MySqlDataAdapter commandAdapter = new MySqlDataAdapter();
                commandAdapter.InsertCommand = new MySqlCommand(_updateTaskStatusQuery, databaseConnection);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"status", status);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"taskId", taskId);
                commandAdapter.InsertCommand.ExecuteNonQuery();
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception}");
                Logger.Error($"Error : {exception}");
                return false;
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return true;
        }
        public bool UpdateTaskResult(String taskId, string result)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }

                MySqlDataAdapter commandAdapter = new MySqlDataAdapter();
                commandAdapter.InsertCommand = new MySqlCommand(_updateTaskResultQuery, databaseConnection);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"result", result);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"taskId", taskId);
                commandAdapter.InsertCommand.ExecuteNonQuery();
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception}");
                Logger.Error($"Error : {exception}");
                return false;
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return true;
        }
        public bool UpdateTaskNode(String taskId, int? nodeId)
        {
            MySqlConnection databaseConnection = DbManager.CreateConnection();
            try
            {
                var connectionStatus = databaseConnection.State.ToString();
                if (connectionStatus != "Open")
                {
                    databaseConnection.Open();
                    Logger.Info("[+] Connection Opened...");
                }

                MySqlDataAdapter commandAdapter = new MySqlDataAdapter();
                commandAdapter.InsertCommand = new MySqlCommand(_updateTaskNodeQuery, databaseConnection);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"nodeId", nodeId);
                commandAdapter.InsertCommand.Parameters.AddWithValue(@"taskId", taskId);
                commandAdapter.InsertCommand.ExecuteNonQuery();
            }
            catch (MySqlException exception)
            {
                Console.WriteLine($"Error : {exception}");
                Logger.Error($"Error : {exception}");
                return false;
            }
            finally
            {
                databaseConnection.Dispose();
                Logger.Info("[-] Connection Disposed...");
                databaseConnection.Close();
                Logger.Info("[x] Connection Closed...");
            }
            return true;
        }

        public List<TaskDTO> GetTasks()
        {
            throw new NotImplementedException();
        }
    }
}