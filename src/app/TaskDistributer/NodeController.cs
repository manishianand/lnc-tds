using System.Net;
using Distributer.DAO.Node;
using Distributer.DAO.Task;
using Utils;

namespace Distributer
{
    public static class NodeController

    {
        public static void StoreNode(IPEndPoint localEndpoint, int nodeId, TaskDTO task)
        {
            NodeDTO node = new NodeDTO();
            node.ip = localEndpoint.Address.ToString();
            node.port = localEndpoint.Port.ToString();
            node.capability = task.taskType;
            node.id = nodeId;
            node.taskId = task.id;
            node.status = Constants.nodeStatus.Busy.ToString();
            NodeImpl nodeImpl = new NodeImpl();
            nodeImpl.InsertNode(node, task);
            Logger.Info($"Node {node.id} Inserted to DB");
        }
    }
}