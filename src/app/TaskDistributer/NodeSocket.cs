using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Distributer.DAO.Client;
using Distributer.DAO.Node;
using Utils;

namespace Distributer
{
    public class NodeSocket
    {
        
        public static string SendToNode(int port, string requestStreamByte)
        {
            try
            {
                IPAddress iPAddress = IPAddress.Parse(Constants.ipAddress);
                IPEndPoint localEndPoint = new IPEndPoint(iPAddress, port);
                Socket sender = new Socket(iPAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                sender.Connect(localEndPoint);

                Console.WriteLine($"Socket connected to : {sender.RemoteEndPoint.ToString()}");
                Logger.Info($"Socket connected to : {sender.RemoteEndPoint.ToString()}");

                var requestObject = ObjectSerializer.Deserialize<ClientDTO>(requestStreamByte);
                var task = requestObject.Task;
                int nodeId = TaskController.EvalNode(task.taskType);

                requestObject.Task.nodeId = nodeId;
                var taskStream = ObjectSerializer.Serialize(task);
                byte[] taskBytes = FileServices.ConstructByteMessage(taskStream);
                int byteSent = sender.Send(taskBytes);
                NodeController.StoreNode(localEndPoint, nodeId, task);
                
                var messageReceivedFromNode = TaskController.ReceiveExecutionResult(sender, requestObject);
                return messageReceivedFromNode;
            }
            catch (ArgumentNullException argumentNullException)
            {
                Console.WriteLine($"ArgumentNullException : {argumentNullException.Message}");
                Logger.Error($"ArgumentNullException : {argumentNullException.Message}");
                return String.Empty;
            }
            catch (SocketException socketException)
            {
                Console.WriteLine($"SocketException : {socketException.Message}");
                Logger.Error($"SocketException : {socketException.Message}");
                return "";
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Unexpected exception : {exception.Message}");
                Logger.Error($"Unexpected exception : {exception.Message}");
                return String.Empty;
            }
        }
    }
}