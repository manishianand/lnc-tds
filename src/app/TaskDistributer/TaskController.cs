using System;
using System.Net;
using System.Net.Sockets;
using Distributer.DAO.Client;
using Distributer.DAO.Task;
using Utils;
using System.Collections.Generic;
using Distributer.DAO.Node;

namespace Distributer
{
    public static class TaskController
    {
        public static void StoreTask(string output, TaskDTO task)
        {
            TaskImpl taskImpl = new TaskImpl();
            var taskID = task.id;
            taskImpl.UpdateTaskNode(taskID, task.nodeId);
            if (output == Constants.taskStatus.TLE.ToString())
            {
                taskImpl.UpdateTaskResult(taskID, Error.timeLimitError);
                taskImpl.UpdateTaskStatus(taskID, Constants.taskStatus.TLE.ToString());
            }
            else if (output.Contains(Constants.taskStatus.Fault.ToString()))
            {
                taskImpl.UpdateTaskResult(taskID, output);
                taskImpl.UpdateTaskStatus(taskID, Constants.taskStatus.Fault.ToString());
            }
            else
            {
                taskImpl.UpdateTaskResult(taskID, output);
                taskImpl.UpdateTaskStatus(taskID, Constants.taskStatus.Complete.ToString());
            }

            Logger.Info($"Task Id {taskID} Inserted to Database");
        }
        public static void ExecuteTask(Socket clientSocket, Queue<ClientDTO> taskQueue)
        {
            var task = taskQueue.Dequeue();
            var rs = ObjectSerializer.Serialize(task);
            var res = NodeSocket.SendToNode(Utils.Constants.serverPort, rs);
            byte[] requestStreamByte = System.Text.Encoding.ASCII.GetBytes(res);
            clientSocket.Send(requestStreamByte);
        }
        public static void QueueTask(Socket clientSocket, string requestStream, IPAddress ipAddress, Queue<ClientDTO> taskQueue)
        {
            var request = ObjectSerializer.Deserialize<Request>(requestStream);
            ClientImpl clientImpl = new ClientImpl();
            request.payload.ip = ipAddress.ToString();
            request.payload.port = Constants.clientPort.ToString();
            clientImpl.InsertClient(request.payload);
            taskQueue.Enqueue(request.payload);
            Console.WriteLine($"Task: {request.payload.taskId}  Queued");
            request.payload.Task.status = Constants.taskStatus.queued.ToString();
            TaskImpl taskImpl = new TaskImpl();
            taskImpl.InsertTask(request.payload.Task);
        }
        public static string ReceiveExecutionResult(Socket sender, ClientDTO requestObject)
        {
            byte[] receivedBytes = new byte[Constants.bufferSize];
            int byteRecv = sender.Receive(receivedBytes);
            string messageReceivedFromNode = System.Text.Encoding.ASCII.GetString(receivedBytes, 0, byteRecv);
            var taskId = requestObject.taskId;
            TaskController.StoreTask(messageReceivedFromNode, requestObject.Task);
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
            NodeImpl nodeImpl = new NodeImpl();
            nodeImpl.UpdateNodeStatus(taskId, Constants.nodeStatus.Available.ToString());
            return messageReceivedFromNode;
        }
        public static int EvalNode(string taskType)
        {
            switch (taskType)
            {
                case "py": return Constants.nodes["py"];
                case "js": return Constants.nodes["js"];
                case "go": return Constants.nodes["go"];
                default: return 0;
            }
        }
    }
}