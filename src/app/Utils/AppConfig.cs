using System;
using System.Collections.Generic;
using System.IO;
using dotenv.net;

namespace Utils
{
    public static class AppConfig
    {
        private static IDictionary<string, string> envVariables;
        public static IDictionary<string, string> LoadConfig()
        {
            string workingDirectory = Directory.GetCurrentDirectory();
            string rootPath = Directory.GetParent(workingDirectory).FullName;
            string envfilePath = rootPath + "\\config.env";
            DotEnv.Load(options: new DotEnvOptions(envFilePaths: new[] { envfilePath }));
            if (envVariables == null)
            {
                envVariables = DotEnv.Read(options: new DotEnvOptions(envFilePaths: new[] { envfilePath }));
            }

            return envVariables;
        }
    }
}