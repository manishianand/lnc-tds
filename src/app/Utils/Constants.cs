using System;
using System.Collections.Generic;
using System.Configuration;

namespace Utils
{
    public class Constants
    {
        public static string ipAddress = AppConfig.LoadConfig()["IpAddress"];
        public static int clientPort = Convert.ToInt32(AppConfig.LoadConfig()["ClientPort"]);
        public static int serverPort = Convert.ToInt32(AppConfig.LoadConfig()["ServerPort"]);
        public static int bufferSize = Convert.ToInt32(AppConfig.LoadConfig()["BufferSize"]);

        public enum taskStatus
        {
            queued,
            Complete,
            Fault,
            TLE
        };
        public enum command
        {
            query,
            execute,
            queue
        }
        public static Dictionary<string, int> nodes = new Dictionary<string, int>(){
            {"py", 100},
            {"js",101},
            {"go",103}
         };
        public enum nodeStatus
        {
            Available,
            Busy,
            Fault
        }
        public const string executeJS = "node file.js> output.txt 2>&1";
        public const string executePython = "python file.py> output.txt 2>&1";
        public const string executeGoLang = "go run  file.go> output.txt 2>&1";
        public const string pythonFile = "\\file.py";
        public const string jsFile = "\\file.js";
        public const string goLangFile = "\\file.go";
        public const string outputFile = "\\output.txt";
        public static string[] validFileTypes = { "c", "cpp", "py", "java", "js", "go" };
    }

}