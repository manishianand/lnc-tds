using System;
namespace Utils
{
    public static class DatabaseConfig
    {
        public static string server = AppConfig.LoadConfig()["Server"];
        public static int port = Convert.ToInt32(AppConfig.LoadConfig()["Port"]);
        public static string userId = AppConfig.LoadConfig()["UserId"];
        public static string database = AppConfig.LoadConfig()["Database"];
        public static string passwd = AppConfig.LoadConfig()["Passwd"];

    }
}