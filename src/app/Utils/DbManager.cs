using MySql.Data.MySqlClient;

namespace Utils
{
    public static class DbManager
    {
        private static MySqlConnection _databaseConnection;
        public static MySqlConnection CreateConnection()
        {
            MySqlConnectionStringBuilder connectionBuilder = new MySqlConnectionStringBuilder();
            connectionBuilder.Server = DatabaseConfig.server;
            connectionBuilder.Port = (uint)DatabaseConfig.port;
            connectionBuilder.UserID = DatabaseConfig.userId;
            connectionBuilder.Password = DatabaseConfig.passwd;
            connectionBuilder.Database = DatabaseConfig.database;
            string builderString = connectionBuilder.ToString();
            connectionBuilder = null;
            if (_databaseConnection == null)
            {
                _databaseConnection = new MySqlConnection(builderString);
            }
            return _databaseConnection;
        }
    }
}