namespace Utils
{
    public static class Error
    {
        public const string noEnvFound = "No runtime environment is found for the file specified";
        public const string invalidFileError = "File type is invalid";
        public const string fileNotFoundError = "File not found !";
        public const string timeLimitError = "Task failed to execute in allowed time";
    }
}