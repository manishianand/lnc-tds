using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading;

namespace Utils
{
    public class FileServices
    {
        public static byte[] ConstructByteMessage(string message)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(message);
            return bytes;
        }
        
        public static void AppendToFile(string file, string content)
        {
            var tries = 5;
            while (true)
            {
                try
                {
                    using (StreamWriter writer = File.AppendText(file))
                    {
                        writer.WriteLineAsync(content);
                        writer.Dispose();
                        writer.Close();
                    }
                    break;
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                    Console.WriteLine("Retrying IO Connection to file....");
                }
            }
        }
        public static string GetLoggerRotDirectory()
        {
            var directory = GetBaseDirectory();
            var rootDirectory = directory + "\\..\\" + "logs.log";
            return rootDirectory;
        }
        public static string GetBaseDirectory()
        {
            string workingDirectory = Directory.GetCurrentDirectory();
            return workingDirectory;
        }
        public static string GetValidFileExtension(string fileName)
        {
            int dotCount = fileName.Count(x => x == '.');
            string fileExtension = fileName.Split('.')[1];
            if (Constants.validFileTypes.Contains(fileExtension) && dotCount == 1)
            {
                return fileExtension;
            }
            return null;
        }
        public static IDictionary<string, string> ParseFileName(string fileName)
        {
            IDictionary<string, string> file = new Dictionary<string, string>();
            string fileExtension = GetValidFileExtension(fileName);
            if (fileExtension != null)
            {
                file.Add("name", fileName);
                file.Add("extention", fileExtension);
                return file;
            }
            return null;
        }
    }
}