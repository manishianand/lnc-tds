using System;
using System.IO;
using System.Threading;

namespace Utils
{
    public static class Logger
    {
        private static string _rootDirectory = FileServices.GetLoggerRotDirectory();
        public static void Info(string info)
        {
            var timeStamp = DateTime.Now.ToString();
            info = "[ " + timeStamp + " ]   " + "Info :" + info;
            FileServices.AppendToFile(_rootDirectory, info);
        }
        public static void Error(string warning)
        {
            var timeStamp = DateTime.Now.ToString();
            warning = "[ " + timeStamp + " ]   " + "Warn :" + warning;
            FileServices.AppendToFile(_rootDirectory, warning);
        }
    }
}