using System;
using Newtonsoft.Json;

namespace Utils
{
    public static class ObjectSerializer
    {
        public static string Serialize(Object data)
        {
            string output = JsonConvert.SerializeObject(data);
            return output;
        }
        public static T Deserialize<T>(string stream)
        {
            T deserializedObject = JsonConvert.DeserializeObject<T>(stream);
            return deserializedObject;
        }
    }
}