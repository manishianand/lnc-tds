using System;
using NUnit.Framework;
using Moq;
using Utils;
using Distributer.DAO.Node;
using Distributer.DAO.Task;
using Distributer.DAO.Client;

namespace tests
{
    public class Tests
    {
        Mock<IClient> clientServiceMock;
        Mock<INode> nodeServiceMock;
        Mock<ITask> taskServiceMock;

        [OneTimeSetUp]
        public void Setup()
        {
            clientServiceMock = new Mock<IClient>();
            nodeServiceMock = new Mock<INode>();
            taskServiceMock = new Mock<ITask>();
        }

        [Test]
        public void GetClientById_ReturnsValidClient()
        {
            clientServiceMock.Setup(_ => _.GetClientById(It.IsAny<int>())).Returns(getSampleClient());

            var actualClient = clientServiceMock.Object.GetClientById(1);
            var expectedClient = getSampleClient();

            Assert.AreEqual(actualClient.id, expectedClient.id);
        }
        [Test]
        public void InsertClient_ValidateSuccess()
        {
            clientServiceMock.Setup(_ => _.InsertClient(It.IsAny<ClientDTO>())).Returns(true);

            var expectedResponse = true;
            var actualResponse = clientServiceMock.Object.InsertClient(getSampleClient());

            Assert.AreEqual(expectedResponse, actualResponse);
        }

        [Test]
        public void InsertNode_ValidateSuccess()
        {
            nodeServiceMock.Setup(_ => _.InsertNode(It.IsAny<NodeDTO>(),It.IsAny<TaskDTO>())).Returns(true);

            var actualResponse = nodeServiceMock.Object.InsertNode(getSampleNode(),getSampleTask());

            Assert.AreEqual(actualResponse, true);
        }

        [Test]
        public void GetNodeById_ReturnsValidNode()
        {
            nodeServiceMock.Setup(_ => _.GetNodeById(It.IsAny<int>())).Returns(getSampleNode());

            var expectedResponse = getSampleNode();
            var actualResponse = nodeServiceMock.Object.GetNodeById(100);

            Assert.AreEqual(actualResponse.id, expectedResponse.id);
        }
        [Test]
        public void GetTaskById_ReturnsValidTask()
        {
            taskServiceMock.Setup(_ => _.GetTaskById(It.IsAny<string>())).Returns(getSampleTask());

            var expectedResponse = getSampleTask();
            var newId = Guid.NewGuid().ToString();
            var actualResponse = taskServiceMock.Object.GetTaskById(newId);

            Assert.AreEqual(actualResponse.id, actualResponse.id);
        }
        [Test]
        public void InsertTask_validateSuccess()
        {
            taskServiceMock.Setup(_ => _.InsertTask(It.IsAny<TaskDTO>())).Returns(true);

            var actualResponse = taskServiceMock.Object.InsertTask(getSampleTask());
            
            Assert.AreEqual(actualResponse, true);
        }

        [Test]
        public void Serialize_ValidateSuccess()
        {
            var nodeData = new NodeDTO
            {
                id = 100,
                status = "Busy",
                ip = "localhost",
                port = "8080"
            };
            var dummyData = new
            {
                payload = nodeData
            };

            var serializedData = ObjectSerializer.Serialize(dummyData);
            Assert.AreNotEqual(serializedData, dummyData);
            Assert.IsTrue(serializedData is string);
        }

        [Test]
        public void Deserialize_ValidateSuccess()
        {
            var nodeData = new NodeDTO
            {
                id = 100,
                status = "Busy",
                ip = "localhost",
                port = "8080"
            };
            var dummyData = new
            {
                payload = nodeData
            };

            var serializedData = ObjectSerializer.Serialize(dummyData);
            var deserializedObject = ObjectSerializer.Deserialize<NodeDTO>(serializedData);
            Assert.IsTrue(deserializedObject is NodeDTO);

        }
        //Private Methods
        private ClientDTO getSampleClient()
        {
            Request request = new Request();
            var guid = new Guid().ToString();
            request.payload = new ClientDTO
            {
                id = 1,
                ip = "127.0.0.1",
                port = "9000",
                taskId = guid,
            };
            return request.payload;
        }
        private NodeDTO getSampleNode()
        {
            NodeDTO nodeSample = new NodeDTO
            {
                id = 100,
                status = "Busy",
                ip = "192.168.43.01",
                port = "9099"
            };
            return nodeSample;
        }
        private TaskDTO getSampleTask()
        {
            TaskDTO taskSample = new TaskDTO
            {
                id = new Guid().ToString(),
                status = "Run Time Error",
                result = "Fail",
                taskType = "C#",
                nodeId = 100
            };
            return taskSample;
        }
    }
}